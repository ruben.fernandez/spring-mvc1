package es.rubenfernandezlago.mvc.data;

import es.rubenfernandezlago.mvc.exception.SpitterNotFoundException;
import es.rubenfernandezlago.mvc.model.Spitter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruben on 11/02/16.
 */
@Component
public class SpitterRepositoryImpl implements SpitterRepository {

    private static List<Spitter> spitterList;

    public SpitterRepositoryImpl() {
        spitterList = new ArrayList<Spitter>();
    }

    @Override
    public void save(Spitter spitter) {
        spitterList.add(spitter);
    }

    @Override
    public Spitter findByUsername(String username) {
        System.out.println("entrooooooo y busco");
        for (Spitter spitter : spitterList) {
            if (spitter.getUsername().equals(username)) {
                return spitter;
            }
        }
        throw new SpitterNotFoundException();
    }
}
