package es.rubenfernandezlago.mvc.data;

import es.rubenfernandezlago.mvc.model.Spittle;

import java.util.List;

/**
 * Created by ruben on 9/02/16.
 */
public interface SpittleRepository {
    List<Spittle> findSpittles(long max, int count);
    Spittle findOne(long id);
}
