package es.rubenfernandezlago.mvc.data;

import es.rubenfernandezlago.mvc.model.Spittle;
import org.apache.tomcat.jni.Time;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ruben on 9/02/16.
 * Esto es una clase temporal que imita a una base de datos, por no complicarme la vida lo hice en memoria todo.
 */
@Component
public class SpittleRepositoryImpl implements SpittleRepository {
    @Override
    public List<Spittle> findSpittles(long max, int count) {
        List<Spittle> splittleList = new ArrayList<Spittle>();
        Date date1 = new Date();
        Spittle spittle1 = new Spittle("Mensaje1111", date1);
        Date date2 = new Date();
        Spittle spittle2 = new Spittle("Mensaje2222", date2);
        splittleList.add(spittle1);
        splittleList.add(spittle2);

        return splittleList;
    }

    @Override
    public Spittle findOne(long id) {
        if (id == 1) {
            Date date = new Date();
            Spittle spittle = new Spittle("Mensaje1111", date);
            return spittle;
        } else if (id == 2) {
            Date date = new Date();
            Spittle spittle = new Spittle("Mensaje2222", date);
            return spittle;
        } else {
            return null;
        }
    }
}
