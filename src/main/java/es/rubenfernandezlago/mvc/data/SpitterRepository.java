package es.rubenfernandezlago.mvc.data;

import es.rubenfernandezlago.mvc.model.Spitter;

/**
 * Created by ruben on 11/02/16.
 */
public interface SpitterRepository {
    void save(Spitter spitter);
    Spitter findByUsername(String username);
}
