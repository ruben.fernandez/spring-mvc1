package es.rubenfernandezlago.mvc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by ruben on 16/02/16.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Spitter Not Found")
public class SpitterNotFoundException extends RuntimeException {
}
