package es.rubenfernandezlago.mvc.dao;

import es.rubenfernandezlago.mvc.model.Role;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rubén on 22/04/2016.
 */
@Repository
public class RoleDAOImpl implements RoleDAO{
    @Autowired
    private SessionFactory sessionFactory;

    public RoleDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public List<Role> list() {
        @SuppressWarnings("unchecked")
        List<Role> listRole = (List<Role>) sessionFactory.getCurrentSession()
                .createCriteria(Role.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

        return listRole;
    }

    @Override
    @Transactional
    public Role get(Long id) {
        String hql = "from Role where id=" + id;
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        @SuppressWarnings("unchecked")
        List<Role> listRole = (List<Role>) query.list();

        if (listRole != null && !listRole.isEmpty()) {
            return listRole.get(0);
        }

        return null;
    }

    @Override
    @Transactional
    public void save(Role role) {
        sessionFactory.getCurrentSession().saveOrUpdate(role);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Role roleToDelete = new Role();
        roleToDelete.setId(id);
        sessionFactory.getCurrentSession().delete(roleToDelete);
    }

    @Override
    @Transactional
    public Role findByName(String name) {
        List<Role> roles = new ArrayList<Role>();

        roles = sessionFactory.getCurrentSession()
                .createQuery("from Role where name=?")
                .setParameter(0, name)
                .list();

        if (roles.size() > 0) {
            return roles.get(0);
        } else {
            return null;
        }

    }

}
