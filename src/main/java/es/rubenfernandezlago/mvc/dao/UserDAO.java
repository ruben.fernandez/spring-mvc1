package es.rubenfernandezlago.mvc.dao;

import es.rubenfernandezlago.mvc.model.User;

import java.util.List;

/**
 * Created by ruben on 29/03/16.
 */
public interface UserDAO {
    public List<User> list();
    public User get(int id);
    public User findByName(String username);
    public User findByEmail(String email);
    public void saveOrUpdate(User user);
    public void delete(int id);
}
