package es.rubenfernandezlago.mvc.dao;

import es.rubenfernandezlago.mvc.model.Role;

import java.util.List;

/**
 * Created by Rubén on 22/04/2016.
 */
public interface RoleDAO {
    public List<Role> list();
    public Role get(Long id);
    public void save(Role role);
    public void delete(Long id);
    public Role findByName(String name);
}
