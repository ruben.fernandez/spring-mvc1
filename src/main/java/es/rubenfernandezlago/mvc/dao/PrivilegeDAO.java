package es.rubenfernandezlago.mvc.dao;

import es.rubenfernandezlago.mvc.model.Privilege;

import java.util.List;

/**
 * Created by Rubén on 22/04/2016.
 */
public interface PrivilegeDAO {
    public List<Privilege> list();
    public Privilege get(Long id);
    public void save(Privilege privilege);
    public void delete(Long id);
    public Privilege findByName(String name);
}
