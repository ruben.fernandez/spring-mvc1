package es.rubenfernandezlago.mvc.dao;

import es.rubenfernandezlago.mvc.model.Privilege;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rubén on 09/05/2016.
 */
@Repository
public class PrivilegeDAOImpl implements PrivilegeDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public PrivilegeDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public List<Privilege> list() {
        @SuppressWarnings("unchecked")
        List<Privilege> listPrivilege = (List<Privilege>) sessionFactory.getCurrentSession()
                .createCriteria(Privilege.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

        return listPrivilege;
    }

    @Override
    @Transactional
    public Privilege get(Long id) {
        String hql = "from Privilege where id=" + id;
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        @SuppressWarnings("unchecked")
        List<Privilege> listPrivilege = (List<Privilege>) query.list();

        if (listPrivilege != null && !listPrivilege.isEmpty()) {
            return listPrivilege.get(0);
        }

        return null;
    }

    @Override
    @Transactional
    public void save(Privilege privilege) {
        sessionFactory.getCurrentSession().saveOrUpdate(privilege);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Privilege privilegeToDelete = new Privilege();
        privilegeToDelete.setId(id);
        sessionFactory.getCurrentSession().delete(privilegeToDelete);
    }


    @Override
    @Transactional
    public Privilege findByName(String name) {
        List<Privilege> privileges = new ArrayList<Privilege>();

        privileges = sessionFactory.getCurrentSession()
                .createQuery("from Privilege where name=?")
                .setParameter(0, name)
                .list();

        if (privileges.size() > 0) {
            return privileges.get(0);
        } else {
            return null;
        }

    }

}
