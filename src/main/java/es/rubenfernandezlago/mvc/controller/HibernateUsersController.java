package es.rubenfernandezlago.mvc.controller;

import es.rubenfernandezlago.mvc.dao.RoleDAO;
import es.rubenfernandezlago.mvc.dao.UserDAO;
import es.rubenfernandezlago.mvc.model.Privilege;
import es.rubenfernandezlago.mvc.model.Role;
import es.rubenfernandezlago.mvc.model.User;
import es.rubenfernandezlago.mvc.service.RoleService;
import es.rubenfernandezlago.mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ruben on 29/03/16.
 */
@Controller
@ComponentScan("es.rubenfernandezlago.mvc.service")
public class HibernateUsersController {
    @Autowired
    private UserDAO userDao;

    @Autowired
    private RoleDAO roleDao;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/hibernate", method = RequestMethod.GET)
    public String listUsers(Model model) {
/*
        for (User u : userService.findAll()) {
            System.out.println("-- NOMBRE -- " + u.getUsername());
            Collection<Role> roles =  u.getRoles();
            for (Role r : roles) {
                System.out.println("ROLES: " + r.getName());
                Collection<Privilege> privileges = r.getPrivileges();
                for (Privilege p : privileges) {
                    System.out.println("PRIVILEGIOS: " + p.getName());
                }
            }
        }
 */

        model.addAttribute("users", userService.findAll());
        return "users/users";
    }

    @RequestMapping(value = "/hibernate/new", method = RequestMethod.GET)
    public String newUser(Model model) {
        model.addAttribute(new User());
        model.addAttribute("roleList", roleService.findAll());
        return "users/crear_usuario";
    }

    @RequestMapping(value = "/hibernate/edit", method = RequestMethod.GET)
    public String editUser(@RequestParam String id, Model model) {
        int userId =  Integer.parseInt(id);
        User user = userDao.get(userId);
        model.addAttribute("user", user);
        model.addAttribute("userRoles",user.getRoles());
        model.addAttribute("roleList", roleService.findAll());

/*
        for (Role role : user.getRoles()) {
            System.out.println("vuelta: " + role.getName());
            if (roleService.findAll().contains(role)) {
                System.out.println("contiene: " + role.getName());
            }
        }
*/
        return "users/editar_usuario";
    }

    @RequestMapping(value = "/hibernate/delete", method = RequestMethod.GET)
    public String deleteUser(@RequestParam String id) {
        int userId =  Integer.parseInt(id);
        userDao.delete(userId);
        return "redirect:/hibernate";
    }

    @RequestMapping(value = "/hibernate/new", method = RequestMethod.POST)
    public String saveUser(@Valid User user, RedirectAttributes model) {
        List<Role> roleList = new ArrayList<Role>();

        for (Role role : user.getRoles()) {
            roleList.add(roleService.findByName(role.getName()));
        }
        user.setRoles(roleList);
        userService.saveOrUpdate(user);
        return "redirect:/hibernate";
    }

    @RequestMapping(value = "/hibernate/edit", method = RequestMethod.POST)
    public String modifyUser(@Valid User user, ArrayList<Role> roles, RedirectAttributes model) {
        List<Role> roleList = new ArrayList<Role>();

        for (Role role : user.getRoles()) {
            System.out.println("## " + role.getName());
            roleList.add(roleService.findByName(role.getName()));
        }
        user.setRoles(roleList);
        //user.setRoles(roles);
        userService.saveOrUpdate(user);
        return "redirect:/hibernate";
    }

}