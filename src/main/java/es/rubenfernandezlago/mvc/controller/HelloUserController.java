package es.rubenfernandezlago.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ruben on 6/03/16.
 */
@Controller
public class HelloUserController {
    @RequestMapping(value="/hellouser", method = RequestMethod.GET)
    public String hellouser() { return "hello/hellouser"; }
}
