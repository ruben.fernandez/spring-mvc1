package es.rubenfernandezlago.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ruben on 9/02/16.
 */
@Controller
@RequestMapping(value={"/spittr","/spittr/homepage"})
public class SpittrHomeController {
    @RequestMapping(method = RequestMethod.GET)
    public String home() {
        return "spittr/spittrhome";
    }
}
