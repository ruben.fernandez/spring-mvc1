package es.rubenfernandezlago.mvc.controller;

/**
 * Created by ruben on 9/02/16.
 */

import es.rubenfernandezlago.mvc.data.SpittleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.websocket.server.PathParam;

@Controller
@ComponentScan(basePackages = {"es.rubenfernandezlago.mvc.data"})
public class SpittleController {
    private SpittleRepository spittleRepository;
    private static final String MAX_LONG_AS_STRING = "200"; //private static final String MAX_LONG_AS_STRING = Long.toString(Long.MAX_VALUE);

    @Autowired
    public SpittleController(SpittleRepository spittleRepository) {
        this.spittleRepository = spittleRepository;
    }

    @RequestMapping(value = "/spittr/spittles", method = RequestMethod.GET)
    public String spittles(@RequestParam(value="max", defaultValue = MAX_LONG_AS_STRING) long max, @RequestParam(value="count", defaultValue = "20") int count, Model model) {
        model.addAttribute("lista", spittleRepository.findSpittles(max, count));
        return "spittr/spittles";
    }

    @RequestMapping(value = "/spittr/spittles/{spittleId}", method = RequestMethod.GET)
    public String spittle(@PathVariable long spittleId, Model model) {
        model.addAttribute("spittle", spittleRepository.findOne(spittleId));
        return "spittr/spittle";
    }
}
