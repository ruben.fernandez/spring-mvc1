package es.rubenfernandezlago.mvc.controller;

import es.rubenfernandezlago.mvc.data.SpitterRepository;
import es.rubenfernandezlago.mvc.model.Spitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Part;
import javax.validation.Valid;
import java.io.IOException;

/**
 * Created by ruben on 11/02/16.
 */
@Controller
@ComponentScan(basePackages = {"es.rubenfernandezlago.mvc.data"})
public class SpitterController {

    private SpitterRepository spitterRepository;

    @Autowired
    private SpitterController(SpitterRepository spitterRepository) {
        this.spitterRepository = spitterRepository;
    }

    @RequestMapping(value = "/spittr/register", method = RequestMethod.GET)
    public String showRegistrationForm(Model model) {
        model.addAttribute(new Spitter());
        return "spittr/registerForm";
    }

    @RequestMapping(value = "/spittr/register", method = RequestMethod.POST)
    public String processRegistration(@RequestPart("profilePicture") Part profilePicture, @Valid Spitter spitter, Errors errors, RedirectAttributes model) throws IOException {
        if (errors.hasErrors()) {
            return "spittr/registerForm";
        }
        profilePicture.write("/tmp/" + profilePicture.getSubmittedFileName());
        spitterRepository.save(spitter);
        model.addAttribute("username",spitter.getUsername());
        model.addFlashAttribute(spitter);
        return "redirect:/spittr/spitter/{username}";
    }

    @RequestMapping(value = "/spittr/spitter/{username}", method = RequestMethod.GET)
    public String showSpitterProfile(@PathVariable String username, Model model) {
        if (!model.containsAttribute("spitter")) {
            Spitter spitter = spitterRepository.findByUsername(username);
            model.addAttribute(spitter);
        }
        return "spittr/profile";
    }
}