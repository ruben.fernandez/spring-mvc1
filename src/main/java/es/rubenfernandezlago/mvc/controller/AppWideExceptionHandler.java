package es.rubenfernandezlago.mvc.controller;

import es.rubenfernandezlago.mvc.exception.SpitterNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by ruben on 16/02/16.
 */
@ControllerAdvice
public class AppWideExceptionHandler {

    @ExceptionHandler(SpitterNotFoundException.class)
    public String duplicateSpittleHandler() {
        return "error/spitternotfound";
    }

}
