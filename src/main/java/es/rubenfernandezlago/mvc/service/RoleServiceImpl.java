package es.rubenfernandezlago.mvc.service;

import es.rubenfernandezlago.mvc.dao.RoleDAO;
import es.rubenfernandezlago.mvc.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by ruben on 6/06/16.
 */
@Component
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDAO roleDao;

    @Override
    public List<Role> findAll() {
        return roleDao.list();
    }

    @Override
    public Role findByName(String name) {
        return roleDao.findByName(name);
    }

    @Override
    public void save(Role role) {
        roleDao.save(role);
    }
}
