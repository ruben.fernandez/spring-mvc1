package es.rubenfernandezlago.mvc.service;

import es.rubenfernandezlago.mvc.dao.UserDAO;
import es.rubenfernandezlago.mvc.model.Privilege;
import es.rubenfernandezlago.mvc.model.Role;
import es.rubenfernandezlago.mvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ruben on 5/06/16.
 */
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDao;

    @Override
    /**
     * @return  Id, Email, Roles, Privileges
     */
    public List<List<String>> findAllWithRolesWithPrivileges() {

        List<User> userList = userDao.list();
        List<String> internalData;
        List<String> privileges = new ArrayList<String>();
        List<List<String>> externalData = new ArrayList<List<String>>();

        for (User user : userList) {
            internalData = new ArrayList<String>();
            internalData.add(String.valueOf(user.getId()));
            internalData.add(user.getEmail());
            Collection<Role> roleList = user.getRoles();
            for (Role role : roleList) {
                internalData.add(role.getName());
                Collection<Privilege> privilegeList = role.getPrivileges();
                for (Privilege privilege : privilegeList) {
                    privileges.add(privilege.getName());
                }
            }
            internalData.addAll(privileges);
            externalData.add(internalData);
        }
        return externalData;
    }

    @Override
    public List<User> findAll() {
        return userDao.list();
    }

    @Override
    public void saveOrUpdate(User user) {
        userDao.saveOrUpdate(user);
    }
}
