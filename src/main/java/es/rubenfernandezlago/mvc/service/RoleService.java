package es.rubenfernandezlago.mvc.service;

import es.rubenfernandezlago.mvc.model.Role;

import java.util.List;

/**
 * Created by ruben on 6/06/16.
 */
public interface RoleService {
    public List<Role> findAll();
    public Role findByName(String nombre);
    public void save(Role role);
}
