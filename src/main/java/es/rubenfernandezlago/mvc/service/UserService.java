package es.rubenfernandezlago.mvc.service;

import es.rubenfernandezlago.mvc.model.User;

import java.util.List;

/**
 * Created by ruben on 5/06/16.
 */
public interface UserService {
    /*public List<User> findAll();
    public User findById(int id);
    public User findByName(String username);
    public User findByEmail(String email);
    public void saveOrUpdate(User user);
    public void remove(int id);*/

    public List<List<String>> findAllWithRolesWithPrivileges();
    public List<User> findAll();
    public void saveOrUpdate(User user);
}
