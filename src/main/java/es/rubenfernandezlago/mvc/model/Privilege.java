package es.rubenfernandezlago.mvc.model;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Rubén on 21/04/2016.
 */
@Entity
@Table(name = "privilege")
public class Privilege {

    private Long id;
    private String name;
    private Collection<Role> roles;

    public Privilege() {

    }

    public Privilege(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "privileges")
    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }
}
