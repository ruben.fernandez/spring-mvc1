package es.rubenfernandezlago.mvc.model;

import javax.persistence.*;

/**
 * Created by ruben on 22/03/16.
 */
@Entity
@Table(name="Persona")
public class Persona {
    Long id;
    String nombre;
    String email;

    public Persona() {
    }

    public Persona(String nombre, String email) {
        this.nombre = nombre;
        this.email = email;
    }

    @Id
    @GeneratedValue
    @Column (name = "PERSONA_ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column (nullable = false)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column (nullable = false, unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
