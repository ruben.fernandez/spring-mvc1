# Spring - MVC1 #

### Resume ###
Learning Spring

### Required ###
* Maven 3.0.5

### Install ###

```
#!bash
apt-get install maven
```

### Configure data base ###
...

### Configure project ###
```
#!bash
mvn clean
mvn compile
mvn package
```

### Launch project ###

```
#!bash
mvn spring-boot:run
```

### Access to web ###
First controller with params:

localhost:8080/hello?name=Ruben

localhost:8080/spittr

### Author Rubén Fernández Lago ###
* [WEB](http://www.rubenfernandezlago.es)
